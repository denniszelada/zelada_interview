class CreateBayEvents < ActiveRecord::Migration
  def change
    create_table :bay_events do |t|
      t.belongs_to :bay, index: true, foreign_key: true
      t.datetime :timestamp
      t.string :type

      t.timestamps null: false
    end
  end
end
