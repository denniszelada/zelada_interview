class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.belongs_to :bay, index: true, foreign_key: true  
      t.datetime :entry_timestamp
      t.datetime :exit_timestamp
      t.integer :dwell

      t.timestamps null: false
    end
    
  end
end
