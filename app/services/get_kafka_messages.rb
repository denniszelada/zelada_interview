class GetKafkaMessages
  
  def initialize(topic)
    @topic = topic
  end

  def get_messages
    consumer = Poseidon::PartitionConsumer.new("messages", "54.172.185.43", 9092, @topic, 0, :earliest_offset)
    messages = consumer.fetch
    messages.each do |m|
      message = eval(m.value)
      save_messages(message)
    end

  end

  def save_messages(message)
    if @topic == 'bay'
      data = {:occupied => message[:occupied], :out_of_service => message[:out_of_service]}
      Bay.where(id = message[:id]).first_or_create.update(data)
    else
      BayEvent.create(message)
    end

  end
end
