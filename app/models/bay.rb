class Bay < ActiveRecord::Base
  has_many :visits
  has_many :bay_events
end
