class MessagesWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { secondly }

  def perform
    bays_service = GetKafkaMessages.new('bays')
    message = bays_service.get_messages
    logger.info("***************************************")
    logger.info('Bays messages')
    logger.info(message)
    logger.info("***************************************")
    bay_events_service = GetKafkaMessages.new('bay_events')
    bay_events_messages = bay_events_service.get_messages
    Rails.logger.info "***************************************"
    Rails.logger.info "Bay Events Messages"
    Rails.logger.info bay_events_messages
    Rails.logger.info "***************************************"
  end
end
