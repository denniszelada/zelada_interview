namespace :kafka do
  task :read => :environment do
    consumer = Poseidon::PartitionConsumer.new("parking_data", "54.172.185.43", 9092, "bays", 0, :earliest_offset)
    loop do
      begin
        messages = consumer.fetch
        #puts "message class"
        #puts messages.class
        messages.each do |m|
          message = eval(m.value)
          Bay.create(message)
          puts "Received message: #{message.class}"
        end
      rescue Poseidon::Errors::UnknownTopicOrPartition
        puts "Topic does not exist yet"
      end

      sleep 1
    end
  end
end
